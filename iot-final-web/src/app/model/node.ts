export class Node {
  constructor(
    public ip: string,
    public id: string,
    public coordinates?: [number, number],
  ) {}
}
