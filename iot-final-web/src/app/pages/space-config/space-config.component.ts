import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MemoryService } from 'src/app/services/memory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-space-config',
  templateUrl: './space-config.component.html',
  styleUrls: ['./space-config.component.scss'],
})
export class SpaceConfigComponent implements OnInit {
  public spaceForm = this.formBuilder.group({
    x: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
    y: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
  });

  constructor(
    private memoryService: MemoryService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {}

  ngOnInit() {}

  get canContinue(): boolean {
    return this.spaceForm.valid;
  }

  continue() {
    if (this.canContinue) {
      const x = this.spaceForm.get('x').value;
      const y = this.spaceForm.get('y').value;
      this.memoryService.setSpace(x, y);
      this.router.navigate(['motes']);
    }
  }
}
