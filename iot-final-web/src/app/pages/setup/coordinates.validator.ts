import {
  ValidatorFn,
  AbstractControl,
  FormArray,
  FormGroup,
  ValidationErrors,
} from '@angular/forms';
import { Coordinate } from 'src/app/model/coordinate';

export function coordinatesValidator(topCoordinate: Coordinate): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const value = control.value;
    const valid =
      value.x >= 0 &&
      value.x < topCoordinate.x &&
      (value.y >= 0 && value.y < topCoordinate.y);
    return !valid ? { invalidCoord: { value: control.value } } : null;
  };
}

export function differentCoordinatesValidator() {
  return (arr: FormArray): ValidationErrors => {
    const values = [];
    const coordinates = arr.controls.map(c => c.get('coord').value);
    const valid = coordinates.every(c => {
      const check = !values.includes(c);
      values.push(c);
      return check;
    });
    return !valid ? { repeatedCoordinates: { value: true } } : null;
  };
}
