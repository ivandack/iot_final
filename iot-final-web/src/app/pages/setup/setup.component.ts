import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MemoryService } from 'src/app/services/memory.service';
import { BackendService } from 'src/app/services/backend.service';
import { Node } from 'src/app/model/node';
import { coordinatesValidator, differentCoordinatesValidator } from './coordinates.validator';
import { Coordinate } from 'src/app/model/coordinate';
import {ThrowStmt} from '@angular/compiler';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss'],
})
export class SetupComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();

  public motes: Node[] = [];
  public coordinatesList: Coordinate[];
  public coordinatesForm = this.formBuilder.group({
    coordinates: this.formBuilder.array([]),
  });

  constructor(
    private memoryService: MemoryService,
    private formBuilder: FormBuilder,
    private backendService: BackendService,
    private router: Router,
  ) {}

  ngOnInit() {
    const arr = this.coordinatesForm.get('coordinates') as FormArray;
    arr.setValidators(differentCoordinatesValidator());
    const space = this.memoryService.getSpace();
    this.coordinatesList = [];
    for (let i = 0; i < space.x; i++) {
      for (let j = 0; j < space.y; j++) {
        this.coordinatesList.push({ x: i, y: j });
      }
    }
    this.motes = this.memoryService.getNodes();
    this.motes.forEach(() => arr.push(this.createCoordinatesSetForm(space)));
  }

  private createCoordinatesSetForm(maxCoordinate: Coordinate): FormGroup {
    return this.formBuilder.group({
      coord: ['', [Validators.required, coordinatesValidator(maxCoordinate)]],
    });
  }

  get canContinue(): boolean {
    return this.coordinatesForm.valid;
  }

  continue() {
    if (this.canContinue) {
      const formArray = this.coordinatesForm.get('coordinates') as FormArray;
      this.motes.forEach((mote, index) => {
        const control = formArray.controls[index];
        mote.coordinates = [
          control.get('coord').value.x,
          control.get('coord').value.y,
        ];
      });
      this.memoryService.saveNodes(this.motes);
      this.subscriptions.add(
        this.backendService
          .sendMotesWithCoordinates(this.motes)
          .subscribe(() => {
            this.router.navigate(['view']);
          }),
      );
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
