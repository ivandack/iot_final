import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MotesPlotComponent } from './motes-plot.component';

describe('MotesPlotComponent', () => {
  let component: MotesPlotComponent;
  let fixture: ComponentFixture<MotesPlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotesPlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotesPlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
