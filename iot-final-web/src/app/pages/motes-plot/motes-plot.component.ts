import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeatmapPoint } from 'src/app/plotly/heatmap-point';
import { MemoryService } from 'src/app/services/memory.service';
import { BackendService } from 'src/app/services/backend.service';
import { Subscription, Subject } from 'rxjs';

@Component({
  selector: 'app-motes-plot',
  templateUrl: './motes-plot.component.html',
  styleUrls: ['./motes-plot.component.scss'],
})
export class MotesPlotComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();

  public data: HeatmapPoint[];
  public plotData: Subject<HeatmapPoint[]> = new Subject();
  public canRefresh = true;
  public spaceSize: {x: number, y: number};

  constructor(
    private memoryService: MemoryService,
    private backendService: BackendService,
  ) {}

  ngOnInit() {
    this.spaceSize = this.memoryService.getSpace();
    this.data = this.memoryService.getNodes()
      .map(
        node => {
          return {
            id: node.id,
            description: `Mota ${node.id}`,
            value: 0,
            coordinates: node.coordinates,
          };
        });
    this.updateValues();
  }

  public updateValues() {
    this.canRefresh = false;
    this.subscriptions.add(
      this.backendService.getLastValues().subscribe(result => {
        console.log(result);
        result.forEach(m => {
          const measurement = this.data.find(d => d.id === m.id);
          if (!measurement) {
            console.error('No se encontró las medidas para la mota ' + m.id);
          }
          measurement.value = m.value;
        });
        this.plotData.next(this.data);
        this.canRefresh = true;
      }),
    );
  }

  get dataJson() {
    return JSON.stringify(this.data);
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
