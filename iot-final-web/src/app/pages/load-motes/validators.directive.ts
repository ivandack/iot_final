import { ValidatorFn, AbstractControl } from '@angular/forms';

const ipv6Pattern = new RegExp(
  [
    '^(::|(([a-fA-F0-9]{1,4}):){7}(([a-fA-F0-9]{1,4}))',
    '|(:(:([a-fA-F0-9]{1,4})){1,6})',
    '|((([a-fA-F0-9]{1,4}):){1,6}:)',
    '|((([a-fA-F0-9]{1,4}):)(:([a-fA-F0-9]{1,4})){1,6})',
    '|((([a-fA-F0-9]{1,4}):){2}(:([a-fA-F0-9]{1,4})){1,5})',
    '|((([a-fA-F0-9]{1,4}):){3}(:([a-fA-F0-9]{1,4})){1,4})',
    '|((([a-fA-F0-9]{1,4}):){4}(:([a-fA-F0-9]{1,4})){1,3})',
    '|((([a-fA-F0-9]{1,4}):){5}(:([a-fA-F0-9]{1,4})){1,2}))$'
  ].join(''), 'gm'
);
const ipv4Pattern = /^(?:(?:^|\.)(?:2(?:5[0-5]|[0-4]\d)|1?\d?\d)){4}$/;

export function validateIp(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const valid =
      ipv6Pattern.test(control.value) || ipv4Pattern.test(control.value);
    return !valid ? { invalidIp: { value: control.value } } : null;
  };
}
