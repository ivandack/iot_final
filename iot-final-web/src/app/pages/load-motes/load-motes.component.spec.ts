import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadMotesComponent } from './load-motes.component';

describe('LoadMotesComponent', () => {
  let component: LoadMotesComponent;
  let fixture: ComponentFixture<LoadMotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadMotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadMotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
