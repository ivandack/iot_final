import { BackendService } from './../../services/backend.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { validateIp } from './validators.directive';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MemoryService } from 'src/app/services/memory.service';

@Component({
  selector: 'app-load-motes',
  templateUrl: './load-motes.component.html',
  styleUrls: ['./load-motes.component.scss'],
})
export class LoadMotesComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  public motes: string[];
  public moteInput = new FormControl('', [Validators.required, validateIp()]);

  public maxMotes: number;

  constructor(
    private backendService: BackendService,
    private memoryService: MemoryService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.motes = [];
    this.memoryService.clearNodes();
    const space = this.memoryService.getSpace();
    this.maxMotes = space.x * space.y;
  }

  addMote() {
    const moteIp: string = this.moteInput.value;
    this.moteInput.reset();
    const values = moteIp.split('\n').filter(s => s.length > 0);
    this.motes.push(...values);
  }

  removeMote(index: number) {
    this.motes.splice(index, 1);
  }

  get canContinue() {
    return this.motes && this.motes.length > 0 && this.motes.length <= this.maxMotes;
  }

  continue() {
    if (this.canContinue) {
      this.subscriptions.add(
        this.backendService.sendMotesIp(this.motes).subscribe(nodes => {
          console.log(nodes);
          this.memoryService.saveNodes(nodes);
          this.router.navigate(['setup']);
        }),
      );
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
