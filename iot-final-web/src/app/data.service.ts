import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private ips: string[] = [];

  constructor() { }

  getIps(): string[] {
    return this.ips.slice();
  }

  setIps(ips: string[]) {
    this.ips = ips;
  }
}
