import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { HeatmapPoint } from 'src/app/plotly/heatmap-point';

const colorscaleValue = [[0, '#0000FF'], [1, '#FF0000']];

@Component({
  selector: 'app-sparseheatmap',
  template: '<plotly-plot [data]="data" [layout]="layout"></plotly-plot>',
  styleUrls: ['./sparseheatmap.component.scss'],
})
export class SparseHeatmapComponent implements OnInit, OnDestroy {
  @Input() size: [number, number];
  @Input() input: Subject<HeatmapPoint[]>;

  data;
  layout;

  private zValues: number[][];

  constructor() {}

  ngOnInit() {
    this.zValues = Array.from({ length: this.size[0] }, (_, i) =>
      Array.from({ length: this.size[1] }, (__, j) => 0),
    );
    const xValues = Array.from(Array(this.size[0]).keys());
    const yValues = Array.from(Array(this.size[1]).keys());

    this.data = [
      {
        x: xValues,
        y: yValues,
        z: this.zValues,
        type: 'heatmap',
        colorscale: colorscaleValue,
        showscale: true,

        zmin: 0,
        zmax: 40,
        zmid: 20,
      },
    ];

    this.layout = {
      title: 'Temperatura del cuarto',
      autosize: true,
      annotations: [],
      xaxis: {
        ticks: '',
        side: 'top',
      },
      yaxis: {
        ticks: '',
        ticksuffix: ' ',
        width: 700,
        height: 700,
        autosize: false,
      },
    };

    for (let i = 0; i < xValues.length; i++) {
      for (let j = 0; j < yValues.length; j++) {
        const currentValue = this.zValues[i][j];
        let textColor;
        if (currentValue < 15) {
          textColor = 'white';
        } else {
          textColor = 'black';
        }
        const result = {
          xref: 'x1',
          yref: 'y1',
          x: xValues[i],
          y: yValues[j],
          transported: true,
          text: '<b>' + this.zValues[i][j] + '</b>',
          font: {
            family: 'Arial',
            size: 14,

            // color: 'rgb(50, 171, 96)',
            color: textColor,
          },
          showarrow: false,
        };
        this.layout.annotations.push(result);
      }
    }

    if (this.input) {
      this.input.subscribe(newValues => {
        this.updateValues(newValues);
      });
    }
  }

  private updateValues(newValues: HeatmapPoint[]) {
    newValues.forEach(p => {
      const [x, y] = p.coordinates;
      this.zValues[y][x] = p.value;
      const annotation = this.layout.annotations.find(r => r.x === x && r.y === y);
      annotation.font.color = (p.value < 25) ? 'white' : 'black';
      annotation.text = '<b>' + p.value + '</b>';
    });
  }

  ngOnDestroy() {
    if (this.input) {
      this.input.unsubscribe();
    }
  }
}
