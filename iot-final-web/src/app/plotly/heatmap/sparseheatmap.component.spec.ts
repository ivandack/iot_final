import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparseHeatmapComponent } from './sparseheatmap.component';

describe('SparseHeatmapComponent', () => {
  let component: SparseHeatmapComponent;
  let fixture: ComponentFixture<SparseHeatmapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparseHeatmapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparseHeatmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
