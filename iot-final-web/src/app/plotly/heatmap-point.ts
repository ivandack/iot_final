export class HeatmapPoint {
  constructor(
    public id: string,
    public description: string,
    public value: number,
    public coordinates: [number, number],
  ) {}
}
