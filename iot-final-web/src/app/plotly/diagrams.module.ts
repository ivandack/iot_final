import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlotlyModule } from 'angular-plotly.js';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { SparseHeatmapComponent } from './heatmap/sparseheatmap.component';
import { BarsComponent } from './bars/bars.component';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [SparseHeatmapComponent, BarsComponent],
  imports: [CommonModule, PlotlyModule],
  exports: [SparseHeatmapComponent, BarsComponent],
})
export class DiagramsModule {}
