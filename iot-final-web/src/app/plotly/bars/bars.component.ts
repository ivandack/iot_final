import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { HeatmapPoint } from '../heatmap-point';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-bars',
  template: '<plotly-plot [data]="data" [layout]="layout"></plotly-plot>',
  styleUrls: ['./bars.component.scss'],
})
export class BarsComponent implements OnInit, OnDestroy {
  @Input() input: Subject<HeatmapPoint[]>;

  public layout;
  public data;

  constructor() {}

  ngOnInit() {
    this.data = [
      {
        type: 'bar',
        x: [],
        y: [],
        marker: {
          color: '#C8A2C8',
          line: {
            width: 2.5,
          },
        },
      },
    ];

    this.layout = {
      title: 'Motas',
      font: { size: 18 },
    };
    if (this.input) {
      this.input.subscribe(newValues => {
        this.updateData(newValues);
      });
    }
  }

  private updateData(newValues: HeatmapPoint[]) {
    this.data[0].x = newValues.map(d => d.id);
    this.data[0].y = newValues.map(d => d.value);
  }

  ngOnDestroy() {
    if (this.input) {
      this.input.unsubscribe();
    }
  }
}
