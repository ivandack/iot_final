import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoadMotesComponent } from './pages/load-motes/load-motes.component';
import { SetupComponent } from './pages/setup/setup.component';
import { MotesPlotComponent } from './pages/motes-plot/motes-plot.component';
import { SpaceConfigComponent } from './pages/space-config/space-config.component';

const routes: Routes = [
  {
    path: '',
    component: SpaceConfigComponent,
  },
  {
    path: 'motes',
    component: LoadMotesComponent,
  },
  {
    path: 'setup',
    component: SetupComponent,
  },
  {
    path: 'view',
    component: MotesPlotComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
