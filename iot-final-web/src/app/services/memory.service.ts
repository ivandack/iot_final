import { Injectable } from '@angular/core';
import {Node} from '../model/node';

@Injectable({
  providedIn: 'root'
})
export class MemoryService {

  constructor() { }

  saveNodes(nodes: Node[]) {
    localStorage.setItem('nodes', JSON.stringify(nodes));
  }

  getNodes(): Node[] {
    return JSON.parse(localStorage.getItem('nodes'));
  }

  clearNodes() {
    localStorage.removeItem('nodes');
  }

  setSpace(x: number, y: number) {
    localStorage.setItem('space', JSON.stringify({x, y}));
  }

  getSpace(): {x: number, y: number} {
    return JSON.parse(localStorage.getItem('space'));
  }

  clearSpace() {
    localStorage.removeItem('space');
  }

}
