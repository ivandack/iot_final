import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Node } from './../model/node';
import { Measurement } from '../model/measurement';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class BackendService {
  private baseUrl = environment.backendHost;

  constructor(private http: HttpClient) {}

  sendMotesIp(motes: string[]): Observable<Node[]> {
    return this.http.post<Node[]>(`${this.baseUrl}/motes`, motes);
  }

  sendMotesWithCoordinates(motes: Node[]): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/motes/coordinates`, motes);
  }

  getLastValues(): Observable<Measurement[]> {
    return this.http.get<Measurement[]>(`${this.baseUrl}/values/current`);
  }
}
