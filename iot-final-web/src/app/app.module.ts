import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatToolbarModule,
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatIconModule,
  MatSelectModule,
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoadMotesComponent } from './pages/load-motes/load-motes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SetupComponent } from './pages/setup/setup.component';
import { HttpClientModule } from '@angular/common/http';
import { MotesPlotComponent } from './pages/motes-plot/motes-plot.component';
import { DiagramsModule } from './plotly/diagrams.module';
import { SpaceConfigComponent } from './pages/space-config/space-config.component';

@NgModule({
  declarations: [
    AppComponent,
    LoadMotesComponent,
    SetupComponent,
    MotesPlotComponent,
    SpaceConfigComponent,
  ],
  imports: [
    DiagramsModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
