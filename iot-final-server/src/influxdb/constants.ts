export enum InfluxConstants {
  MEASUREMENT = 'temperature',
  TAG_NAME = 'nodeId',
  FIELD_NAME = 'value',
}
