import { InfluxDB, IResults, INanoDate } from 'influx';
import { Measurement } from 'src/model/measurement';
import { Node } from 'src/model/node';
import { InfluxConstants } from './constants';

interface QueryResult {
  nodeId: string;
  x: string;
  y: string;
  value: number;
  time: INanoDate;
}

export class InfluxService {
  constructor(private db: InfluxDB) {}

  async saveData(node: Node, value: number) {
    await this.db.writePoints([
      {
        measurement: InfluxConstants.MEASUREMENT,
        tags: {
          nodeId: node.id,
          x: '' + node.coordinates[0],
          y: '' + node.coordinates[1],
        },
        fields: {
          value,
        },
      },
    ]);
  }

  async getCurrent(): Promise<Measurement[]> {
    const result = await this.db.query<
      QueryResult
    >(`select nodeId, x, y, last(value) as value \
                   from "${InfluxConstants.MEASUREMENT}" \
                   group by nodeId, x, y`);
    return result.map(
      r => new Measurement(r.nodeId, r.value, new Date(r.time)),
    );
  }
}
