import { Provider } from '@nestjs/common';
import { ISingleHostConfig, FieldType, InfluxDB } from 'influx';
import { InfluxService } from './influx.service';
import { InfluxConstants } from './constants';

export function createInfluxProvider(options: ISingleHostConfig): Provider {
  options.schema = [
    {
      measurement: InfluxConstants.MEASUREMENT,
      tags: ['nodeId', 'x', 'y'],
      fields: {
        value: FieldType.INTEGER,
      },
    },
  ];

  const influx = new InfluxDB(options);
  const service = new InfluxService(influx);

  influx
    .getDatabaseNames()
    .catch(err => {
      throw new Error(err);
    })
    .then(databaseNames => {
      if (!databaseNames.includes(options.database)) {
        influx.createDatabase(options.database);
      }
    })
    .catch(() => {
      throw new Error(`Error creando base de datos "${options.database}"!`);
    });

  return {
    provide: InfluxService,
    useValue: service,
  };
}
