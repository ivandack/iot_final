import { Module, DynamicModule } from '@nestjs/common';
import { ISingleHostConfig } from 'influx';
import { InfluxService } from './influx.service';
import { createInfluxProvider } from './influx.providers';

@Module({
  providers: [InfluxService],
})
export class InfluxdbModule {
  static forRoot(options: ISingleHostConfig): DynamicModule {
    const provider = createInfluxProvider(options);
    return {
      module: InfluxdbModule,
      providers: [provider],
      exports: [provider],
    };
  }
}
