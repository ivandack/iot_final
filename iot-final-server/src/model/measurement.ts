export class Measurement {
  constructor(
    public id: string,
    public value: number,
    public timestamp: Date,
    public coordinates?: [number, number],
  ) {}
}
