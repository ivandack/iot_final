import { Module } from '@nestjs/common';
import { CoapService } from './coap.service';

@Module({
  providers: [CoapService],
  exports: [CoapService],
})
export class CoapModule {}
