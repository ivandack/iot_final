import { Test, TestingModule } from '@nestjs/testing';
import { CoapService } from './coap.service';

describe('CoapService', () => {
  let service: CoapService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoapService],
    }).compile();

    service = module.get<CoapService>(CoapService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
