import * as coapLib from 'ivandack-coap-cli/src/lib/coap-lib';

export class CoapHost {
  constructor(public host: string, public port?: number) {}

  async get(path: string): Promise<string> {
    return coapLib.get(this.host, this.port, path);
    //return Promise.resolve('' + (Math.random() * (30 - 4) + 4));
  }

  async post(path: string, body: any): Promise<string> {
    return coapLib.post(this.host, this.port, path, body);
  }
}
