import { Injectable } from '@nestjs/common';
import { CoapHost } from './coap-host';

@Injectable()
export class CoapService {
  fromHost(host: string, port?: number) {
    return new CoapHost(host, port);
  }
}
