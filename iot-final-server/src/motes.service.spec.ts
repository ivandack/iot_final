import { Test, TestingModule } from '@nestjs/testing';
import { MotesService } from './motes.service';

describe('MotesService', () => {
  let service: MotesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MotesService],
    }).compile();

    service = module.get<MotesService>(MotesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
