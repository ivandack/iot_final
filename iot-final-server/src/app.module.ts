import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CoapModule } from './coap/coap.module';
import { InfluxdbModule } from './influxdb/influxdb.module';
import { MotesService } from './motes.service';

@Module({
  imports: [
    CoapModule,
    InfluxdbModule.forRoot({
      host: process.env.INFLUXDB_HOST,
      database: 'iot_final',
    }),
  ],
  controllers: [AppController],
  providers: [MotesService],
})
export class AppModule {}
