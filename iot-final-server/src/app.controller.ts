import {
  Controller,
  Get,
  Post,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { CoapService } from './coap/coap.service';
import { Node } from './model/node';
import { Measurement } from './model/measurement';
import { MotesService } from './motes.service';
import { InfluxService } from './influxdb/influx.service';

@Controller()
export class AppController {
  private execution: NodeJS.Timeout;

  constructor(
    private readonly coapService: CoapService,
    private readonly motesService: MotesService,
    private readonly influxService: InfluxService,
  ) {}

  @Post('/motes')
  async loadMotes(@Body() body: string[]): Promise<Node[]> {
    if (!body || !Array.isArray(body)) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
    const dataPromises = body.map(ip =>
      this.coapService
        .fromHost(ip)
        .get('/id')
        .then(id => new Node(ip, id)),
    );
    return Promise.all(dataPromises).then(res => this.motesService.setAllNodes(res));
  }

  @Post('/test/motes')
  async loadMockMotes(@Body() body: string[]): Promise<Node[]> {
    if (!body || !Array.isArray(body)) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
    const data = body.map((ip, index) => {
      return new Node(ip, '' + (index + 1));
    });
    return this.motesService.setAllNodes(data);
  }

  @Post('/motes/coordinates')
  async loadCoordinates(@Body() body: Node[]) {
    if (!body || !Array.isArray(body)) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
    this.motesService.setAllNodes(body);
    this.start();
  }

  @Post('/start')
  start() {
    this.execution = setInterval(() => {
      this.motesService.getAll().forEach(node => {
        this.coapService
          .fromHost(node.ip)
          .get('/environment/temperature')
          .then(value => {
            this.influxService.saveData(node, (Math.random() * (30 - 4) + 4));
          });
      });
    }, 5000);
  }

  @Post('/stop')
  stop() {
    if (this.execution) {
      clearInterval(this.execution);
    }
  }

  @Get('/values/current')
  async getCurrent(): Promise<Measurement[]> {
    return this.influxService.getCurrent();
  }
}
