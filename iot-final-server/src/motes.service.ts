import { Injectable } from '@nestjs/common';
import { Node } from './model/node';

@Injectable()
export class MotesService {
  private nodes: Node[] = [];

  constructor() {
    this.nodes = [];
  }

  addNode(node: Node) {
    this.nodes.push(node);
  }

  setAllNodes(nodesToAdd: Node[]): Node[] {
    this.clearNodes();
    if (nodesToAdd) {
      nodesToAdd.forEach(this.addNode, this);
      return this.nodes.slice();
    }
  }

  clearNodes() {
    this.nodes = [];
  }

  getNodeById(id: string): Node {
    return this.nodes.find((node) => node.id === id);
  }

  getAll(): Node[] {
    return this.nodes.slice();
  }

  removeNode(node: Node) {
    const index = this.nodes.indexOf(node);
    if (index > -1) {
      this.nodes = this.nodes.splice(index, 1);
    }
  }
}
